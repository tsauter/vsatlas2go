package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	consulapi "github.com/hashicorp/consul/api"
	"github.com/jcelliott/lumber"
	"github.com/prometheus/client_golang/prometheus"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"time"
)

var (
	listenAddress           = flag.String("listen-address", ":8000", "Address for serving box files.")
	listenAddressPrometheus = flag.String("listen-prometheus", ":9001", "Listen address to server prometheus metrics.")
	localBoxFilepath        = flag.String("filepath", "d:\\localrepo", "")
	consulAddress           = flag.String("consul-address", "", "Register instances on Consul server.")
	useDebug                = flag.Bool("debug", false, "")
	showVersion             = flag.Bool("version", false, "show version and exit")

	Version   = "xx"
	BuildTime = "xx"
	BuildUser = "xx"
	BuildHost = "xx"

	log = lumber.NewConsoleLogger(lumber.INFO)

	fs http.Handler

	metricsTotalJsonRequests = prometheus.NewCounter(prometheus.CounterOpts{
		Namespace: "vsatlas",
		Name:      "json_requests_total",
		Help:      "Total number of JSON requestJSON requests.",
	})
	metricsTotalBoxDownloads = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: "vsatlas",
			Name:      "box_downloads_total",
			Help:      "Total number of JSON requestJSON requests.",
		},
		[]string{"box"},
	)
	metricsBoxDownloadSummary = prometheus.NewSummaryVec(
		prometheus.SummaryOpts{
			Namespace: "vsatlas",
			Name:      "box_downloads",
			Help:      "Summary of box downloads.",
		},
		[]string{"box", "code"},
	)
)

type ProviderDetails struct {
	Name          string `json:"name"`
	Url           string `json:"url"`
	Checksum_type string `json:"checksum_type"`
	Checksum      string `json:"checksum"`
}

type BoxVersion struct {
	Version   string            `json:"version"`
	Providers []ProviderDetails `json:"providers"`
}

type BoxInfo struct {
	Name        string       `json:"name"`
	Description string       `json:"description"`
	Versions    []BoxVersion `json:"versions"`
}

type BoxDownloadDetails struct {
	Id           uint64 `json:"id"`
	Url          string `json:"url"`
	Checksum     string `json:"checksum"`
	ChecksumType string `json:"checksum_type"`
}

func handleJsonRequest(w http.ResponseWriter, r *http.Request) {
	host := fmt.Sprintf("%s://%s", "http", r.Host)

	pathParts := strings.Split(r.URL.Path, "/")
	if len(pathParts) < 9 {
		fmt.Printf("Invalid request: %s\n", r.URL.Path)
		return
	}
	repository := pathParts[6]
	box := pathParts[8]

	fmt.Printf("Incoming JSON request: %s: %s: %s/%s\n", host, r.URL.Path, repository, box)
	data, _ := returnBoxJson(host, repository, box)
	io.WriteString(w, string(data))
}

func handleBoxIndexJson(w http.ResponseWriter, r *http.Request) {
	log.Debug("Serving JSON index for boxes in directory: %s\n", *localBoxFilepath)

	metricsTotalJsonRequests.Inc()

	// Search for all available boxes in the specified directory.
	files, err := filepath.Glob(*localBoxFilepath + "\\[1-9]*[1-9]")
	if err != nil {
		fmt.Printf("Failed to search box in directory: %s: %s\n", *localBoxFilepath, err)
		return
	}
	if files == nil {
		log.Warn("No boxes found in directory: %s", *localBoxFilepath)
	}
	log.Debug("Found %d boxes in %s", len(files), *localBoxFilepath)

	/*
		type BoxDownloadInfo struct {
			Id           uint64 `json:"id"`
			Url          string `json:"url"`
			Checksum     string `json:"checksum"`
			ChecksumType string `json:"checksum_type"`
		}
	*/

	for _, filename := range files {
		shortfilename := path.Base(filename)

		var boxi BoxDownloadDetails
		boxi.Id, err = strconv.ParseUint(shortfilename, 10, 10)
		boxi.Url = "x"
		boxi.Checksum = filename
		boxi.ChecksumType = "dd"
		fmt.Printf("%v\n", boxi)
	}

	host := fmt.Sprintf("%s://%s", "http", r.Host)

	pathParts := strings.Split(r.URL.Path, "/")
	if len(pathParts) < 9 {
		fmt.Printf("Invalid request: %s\n", r.URL.Path)
		return
	}
	repository := pathParts[6]
	box := pathParts[8]

	fmt.Printf("Incoming JSON request: %s: %s: %s/%s\n", host, r.URL.Path, repository, box)
	data, _ := returnBoxJson(host, repository, box)
	io.WriteString(w, string(data))
}

func returnBoxJson(hostUri string, repository string, box string) ([]byte, error) {
	basepath := filepath.Join(*localBoxFilepath, repository, box)
	fmt.Printf("Serving box from: %s\n", basepath)

	// Search for all available boxes in the specified directory: repository/box/0.1__virtualbox.box
	//files, err := ioutil.ReadDir(basepath)
	files, err := filepath.Glob(basepath + "\\*.box")
	if err != nil {
		fmt.Printf("Failed to search box in directory: %s: %s\n", basepath, err)
		return nil, err
	}

	var hashAlgorithm = "sha1"
	var nameregex = regexp.MustCompile(`(?P<repository>.*)__(?P<box>.*)__(?P<version>.*)__(?P<provider>.*)\.box`)

	// Create basic structure for this box, we use this structure to marshal to json later
	var boxinfo BoxInfo
	boxinfo.Name = box
	boxinfo.Description = "n/a (vsatlas2go)"

	for _, f := range files {
		//fmt.Printf("%s\n", f)
		// 1==repository
		// 2==box
		// 3==version
		// 4==provider
		parts := nameregex.FindStringSubmatch(f)
		//fmt.Printf("%+v\n", parts)

		// Try to load the corresponding checksum
		checksum_filename := fmt.Sprintf("%s.%s", f, hashAlgorithm)
		checksum_content, err := ioutil.ReadFile(checksum_filename)
		if err != nil {
			fmt.Printf("Unable to read checksum from file: %s: %s\n", checksum_filename, err)
			checksum_content = []byte{} // should we break here?
			return nil, err
		}

		var version BoxVersion
		version.Version = parts[3]

		downloadPath := hostUri
		downloadPath += "/vsatlas/registry/api/v1/download/boxes/"
		downloadPath += fmt.Sprintf("%s/%s/", repository, box)
		downloadPath += filepath.Base(f)

		var provider ProviderDetails
		provider.Name = parts[4]
		provider.Url = downloadPath
		provider.Checksum_type = hashAlgorithm
		provider.Checksum = strings.TrimSpace(string(checksum_content))

		version.Providers = append(version.Providers, provider)

		boxinfo.Versions = append(boxinfo.Versions, version)
	}

	//fmt.Println("%v", boxinfo)

	// Create JSON output
	data, err := json.Marshal(boxinfo)
	if err != nil {
		fmt.Printf("Failed to marshal json: %s\n", err)
		return nil, err
	}
	return data, err
}

/*
 * The structs and interfaces overwrites the ServeHTTP stuff.
 * This is required to get the http return code
 */

type BoxHandler struct {
}

type StatusResponseWriter struct {
	status         int
	ResponseWriter http.ResponseWriter
}

func (w *StatusResponseWriter) WriteHeader(code int) {
	w.status = code
	w.ResponseWriter.WriteHeader(code)
}

func (w *StatusResponseWriter) Header() http.Header {
	return w.ResponseWriter.Header()
}

func (w *StatusResponseWriter) Write(bytes []byte) (int, error) {
	return w.ResponseWriter.Write(bytes)
}

func (bh BoxHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	boxid := r.URL.String()

	metricsTotalBoxDownloads.WithLabelValues(boxid).Inc()

	fmt.Printf("Box download request: %s\n", boxid)

	start := time.Now()

	srw := StatusResponseWriter{-1, w}
	fs.ServeHTTP(&srw, r)
	elapsed := time.Since(start)

	elapsed_seconds := float64(elapsed / time.Second)
	metricsBoxDownloadSummary.WithLabelValues(boxid, strconv.Itoa(srw.status)).Observe(elapsed_seconds)
}

func main() {
	flag.Parse()

	if *showVersion {
		fmt.Printf("vsatlas2go: v%s (builded at %s on %s@%s)\n", Version,
			BuildTime, BuildUser, BuildHost)
		os.Exit(1)
	}

	if *useDebug {
		log.Level(lumber.DEBUG)
	}

	// Create a handler for JSON files, this is not used at the moment
	//http.HandleFunc("/vsatlas/registry/api/v1/vagrant/", handleJsonRequest)

	http.HandleFunc("/vsatlas/registry/api/v1/download/boxindex/", handleBoxIndexJson)

	// Create a simple and efficient web server to server static files in the specified directory
	// /vsatlas/registry/api/v1/download/boxes/60
	fmt.Printf("Loading boxes from %s\n", *localBoxFilepath)
	fs = http.FileServer(http.Dir(*localBoxFilepath))
	boxHandler := BoxHandler{}
	baseurl := "/vsatlas/registry/api/v1/download/boxes/"
	http.Handle(baseurl, http.StripPrefix(baseurl, boxHandler))

	prometheus.MustRegister(metricsTotalJsonRequests)
	prometheus.MustRegister(metricsTotalBoxDownloads)
	prometheus.MustRegister(metricsBoxDownloadSummary)
	http.Handle("/metrics", prometheus.Handler())
	go func() {
		fmt.Printf("Prometheus metrics on %s\n", *listenAddressPrometheus)
		if err := http.ListenAndServe(*listenAddressPrometheus, nil); err != nil {
			panic(err)
		}
	}()

	if *consulAddress != "" {
		consulTicker := time.NewTicker(60 * time.Second)
		consulTickerQuit := make(chan bool)
		go func() {

			for {
				select {
				case <-consulTicker.C:
					RegisterConsoleOnTimer()
				case <-consulTickerQuit:
					consulTicker.Stop()
					return
				}
			}

		}()

		RegisterConsoleOnTimer()
	}

	fmt.Printf("Starting new vsatlas Repository server on %s...\n", *listenAddress)
	if err := http.ListenAndServe(*listenAddress, nil); err != nil {
		panic(err)
	}

}

func RegisterConsoleOnTimer() {
	if *useDebug {
		fmt.Printf("Registing on Consul: %s\n", *consulAddress)
	}

	if err := RegisterConsul("vsatlas-repository", *listenAddress, true); err != nil {
		fmt.Printf("Failed to register on consul: %s: %s\n", *consulAddress, err.Error())
	}
	if err := RegisterConsul("prometheus", *listenAddressPrometheus, false); err != nil {
		fmt.Printf("Failed to register on consul: %s: %s\n", *consulAddress, err.Error())
	}
}

func RegisterConsul(name string, laddr string, autoDeregister bool) error {
	if *consulAddress == "" {
		return errors.New("Missing consul address. Aborting registration.")
	}

	consulCfg := consulapi.DefaultConfig()
	// default Consul address: 127.0.0.1:8500
	consulCfg.Address = *consulAddress
	consul, err := consulapi.NewClient(consulCfg)
	if err != nil {
		return err
	}

	// Extract the hostname/port from listen address, this is required
	// to register myself at consul instance
	host, port_raw, err := net.SplitHostPort(laddr)
	if err != nil {
		return err
	}

	if host == "" {
		host, err = os.Hostname()
		if err != nil {
			return err
		}
	}

	port, err := strconv.Atoi(port_raw)
	if err != nil {
		return err
	}

	check := consulapi.AgentServiceCheck{
		//HTTP:     fmt.Sprintf("http://%s:%d/", host, port),
		TCP:      fmt.Sprintf("%s:%d", host, port),
		Interval: "10s",
		Timeout:  "1s",
	}
	if autoDeregister {
		check.DeregisterCriticalServiceAfter = "5m"
	}

	reg := &consulapi.AgentServiceRegistration{ID: host + ":" + port_raw, Name: name, Address: host, Port: port, Check: &check}
	if err := consul.Agent().ServiceRegister(reg); err != nil {
		return err
	}

	return nil
}
